<?php

namespace Drupal\views_pretty_paths\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\redirect\EventSubscriber\RedirectRequestSubscriber;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Decorates the original event subscriber.
 */
class RedirectRequestSubscriberDecorator implements EventSubscriberInterface {

  const DEFAULT_FILTER_SUBPATH = '/filter';

  /**
   * The decorated original event subscriber.
   *
   * @var \Drupal\redirect\EventSubscriber\RedirectRequestSubscriber
   */
  protected $decoratedOriginalSubscriber;

  /**
   * Views Pretty Path config.
   *
   * @var array
   */
  protected $config;

  /**
   * RedirectRequestSubscriberDecorator constructor.
   *
   * @param \Drupal\redirect\EventSubscriber\RedirectRequestSubscriber $decoratedOriginalSubscriber
   *   Original RedirectRequestSubscriber service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   */
  public function __construct(RedirectRequestSubscriber $decoratedOriginalSubscriber, ConfigFactoryInterface $configFactory) {
    $this->decoratedOriginalSubscriber = $decoratedOriginalSubscriber;
    $this->config = $configFactory->get('views_pretty_paths.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Override or extend the subscribed events as needed.
    $events[KernelEvents::REQUEST][] = ['onKernelRequestCheckRedirect', 33];
    return $events;
  }

  /**
   * Handles the redirect if any found.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function onKernelRequestCheckRedirect(RequestEvent $event) {
    $request = $event->getRequest();
    // Prevent redirect module to redirect.
    if ($this->shouldPathBeRewritten($request->getRequestUri())) {
      return FALSE;
    }
    return $this->decoratedOriginalSubscriber
      ->onKernelRequestCheckRedirect($event);
  }

  /**
   * Check if path needs to be rewritten according to config.
   *
   * @param string $path
   *   Current request path.
   */
  protected function shouldPathBeRewritten($path): bool {
    // Get filter subPath.
    $filterSubPath = '/' . ltrim($this->config->get('filter_subpath') ? $this->config->get('filter_subpath') : self::DEFAULT_FILTER_SUBPATH, '/');
    $paths_views_to_rewrite = [];
    $paths = $this->config->get('paths') ? $this->config->get('paths') : [];
    foreach ($paths as $confPath) {
      $paths_views_to_rewrite[$confPath['path']] = $confPath;
    }
    foreach (array_keys($paths_views_to_rewrite) as $path_to_rewrite) {
      if (
        $path == $path_to_rewrite ||
        strtok($path, '?') == $path_to_rewrite ||
        strpos($path, $path_to_rewrite . $filterSubPath) === 0
      ) {
        return $path_to_rewrite;
      }
    }
    return FALSE;
  }

}
