<?php

namespace Drupal\views_pretty_paths\FilterHandlers;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;

/**
 * Defines AbstractFilterHandler class.
 */
abstract class AbstractFilterHandler implements ViewsPrettyPathFilterHandlerInterface {

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs AbstractFilterHandler object.
   */
  public function __construct(Connection $connection) {
    $this->database = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetedFilterPluginIds() {
  }

  /**
   * {@inheritdoc}
   */
  public function transformPathValueForViewsQuery($filter_value_string, $filter_data) {
  }

  /**
   * {@inheritdoc}
   */
  public function transformSubmittedValueForUrl($value) {
  }

  /**
   * Encode multiple words.
   *
   * @param string $words_string
   *   Get words.
   * @param string $delimiter
   *   Delimiter to use.
   */
  protected function encodeMultipleWordsForUrl($words_string, $delimiter = '-') {
    $words_array = explode(' ', $words_string);
    $encoded_words_array = array_map(function ($word) {
      return $this->encodeWordForUrl($word);

    }, $words_array);
    return implode($delimiter, $encoded_words_array);
  }

  /**
   * Encode a single word.
   *
   * @param string $word
   *   Given word.
   *
   * @return string
   *   Encoded word.
   */
  protected function encodeWordForUrl($word) {
    $processed_word = trim(strtolower($word));
    $processed_word = implode('-', array_map(function ($word_item) {
      return UrlHelper::encodePath($word_item);
    }, explode('-', $processed_word)));
    return $processed_word;
  }

  /**
   * Decode a single word.
   *
   * @param string $word
   *   Given word.
   *
   * @return string
   *   Decoded word.
   */
  protected function decodeUrlWord($word) {
    return urldecode($word);
  }

}
