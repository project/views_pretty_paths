<?php

namespace Drupal\views_pretty_paths\FilterHandlers;

/**
 * Defines BundleFilterHandler class.
 */
class BundleFilterHandler extends AbstractFilterHandler implements ViewsPrettyPathFilterHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function getTargetedFilterPluginIds() {
    return [
      'bundle',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transformPathValueForViewsQuery($filter_value_string, $filter_data) {
    $raw_values = explode('+', $filter_value_string);
    $return_values = [];
    foreach ($raw_values as $raw_value) {
      $return_values[$raw_value] = $raw_value;
    }
    return $return_values;
  }

  /**
   * {@inheritdoc}
   */
  public function transformSubmittedValueForUrl($value) {
    return implode('+', $value);
  }

}
