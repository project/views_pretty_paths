<?php

namespace Drupal\views_pretty_paths\FilterHandlers;

/**
 * Defines TextFilterHandler class.
 */
class TextFilterHandler extends AbstractFilterHandler implements ViewsPrettyPathFilterHandlerInterface {

  /**
   * {@inheritdoc}
   */
  public function getTargetedFilterPluginIds() {
    return [
      'search_keywords',
      'search_api_fulltext',
      'combine',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function transformPathValueForViewsQuery($filter_value_string, $filter_data) {
    $raw_values = explode('+', $filter_value_string);
    $return_text = '';
    if (!empty($raw_values)) {
      foreach ($raw_values as $raw_value) {
        $return_text = $return_text . ' ' . $this->decodeUrlWord($raw_value);
      }
      $return_text = ltrim($return_text);
    }
    return $return_text;
  }

  /**
   * {@inheritdoc}
   */
  public function transformSubmittedValueForUrl($value) {
    return $this->encodeMultipleWordsForUrl($value, '+');
  }

}
