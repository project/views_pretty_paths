<?php

namespace Drupal\views_pretty_paths\FilterHandlers;

/**
 * Defines ViewsPrettyPathFilterHandlerInterface interface.
 */
interface ViewsPrettyPathFilterHandlerInterface {

  /**
   * Converts a URL string value for a filter into a value the the Request.
   *
   * @param mixed $filter_value_string
   *   Given value.
   * @param mixed $filter_data
   *   Filtered data.
   *
   * @return string|array
   *   Returnes converted url value.
   */
  public function transformPathValueForViewsQuery($filter_value_string, $filter_data);

  /**
   * Converts the form submitted value for a filter into a string for the URL.
   *
   * @param string|array $values
   *   Form submitted values.
   *
   * @return string
   *   Converted string.
   */
  public function transformSubmittedValueForUrl($values);

  /**
   * Returns array of filter plugin ID strings that the filter handler targets.
   */
  public function getTargetedFilterPluginIds();

}
