<?php

namespace Drupal\views_pretty_paths\PathProcessor;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\PathProcessor\InBoundPathProcessorInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\views_pretty_paths\FilterHandlers\ViewsPrettyPathFilterHandlerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Rewrite Views URLs to be human-readable.
 */
class ViewsPrettyPathProcessor implements InBoundPathProcessorInterface, OutboundPathProcessorInterface {

  const DEFAULT_FILTER_SUBPATH = '/filter';

  /**
   * Whether the outbound path has been changed at least once.
   *
   * @var bool
   */
  protected $changedOutboundOnce;

  /**
   * Whether the inbound path has been changed at least once.
   *
   * @var bool
   */
  protected $changedInboundOnce;

  /**
   * The inbound path of the first path that required rewritten.
   *
   * @var string
   */
  protected $originalRewrittenInboundPath;

  /**
   * Alias manager service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Collection of filter handlers, keyed by targeted filter plugin ID.
   *
   * @var array
   */
  protected $filterHandlers;

  /**
   * Maps paths to view names where URL rewriting should be enabled.
   *
   * - Modules can use hook_views_paths_to_rewrite_alter() to override
   * - Keys: paths, values: view names.
   *
   * @var array
   */
  protected $pathsViewsToRewrite;

  /**
   * Map that keys view field names to user-defined override names.
   *
   * - Modules can use hook_views_field_name_map_alter() to override
   * - Increases user-friendliness of a URL
   * - e.g. ['field_topic_target_id' => 'topics']
   *
   * @var false|array
   */
  protected $viewsFieldNameMap = FALSE;

  /**
   * Query parameters stored internally.
   *
   * @var array
   */
  protected $queryParameters = [];

  /**
   * Form state of submitted form.
   *
   * @var \Drupal\Core\Form\FormStateInterface|null
   */
  protected $formState;

  /**
   * Views Pretty Path config.
   *
   * @var array
   */
  protected $config;

  /**
   * Filter subpath.
   *
   * @var string
   */
  protected $filterSubpath;

  /**
   * Constructs ViewsPrettyPathProcessor object.
   */
  public function __construct(
    AliasManagerInterface $aliasManager,
    EntityTypeManagerInterface $entityTypeManager,
    Connection $connection,
    RequestStack $requestStack,
    ConfigFactoryInterface $configFactory) {
    $this->aliasManager = $aliasManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $connection;
    $this->changedOutboundOnce = FALSE;
    $this->changedInboundOnce = FALSE;
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->config = $configFactory->get('views_pretty_paths.config');
    $this->filterSubpath = '/' . ltrim($this->config->get('filter_subpath') ? $this->config->get('filter_subpath') : self::DEFAULT_FILTER_SUBPATH, '/');
    $this->pathsViewsToRewrite = $this->loadPathsViewsToRewrite();
  }

  /**
   * Add a filter handler to the filterHandlers property.
   *
   * @param \Drupal\views_pretty_paths\FilterHandlers\ViewsPrettyPathFilterHandlerInterface $filter_handler
   *   Filter handler.
   * @param int $priority
   *   Priority.
   */
  public function addFilterHandler(ViewsPrettyPathFilterHandlerInterface $filter_handler, $priority = 0) {
    foreach ($filter_handler->getTargetedFilterPluginIds() as $plugin_id) {
      $this->filterHandlers[$plugin_id] = $filter_handler;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $new_path = $path;
    // Check if the path should be rewritten.
    if ($alias = $this->shouldPathBeRewritten($request->getRequestUri())) {
      // Ensure the filter subpath is in the path.
      // and that the alias matches a system path.
      if (($this->isFilterInPath($alias, $request)) && ($system_path_of_alias = $this->aliasManager->getPathByAlias($alias))) {
        $this->setViewsFieldNameMap();
        if (!$this->changedInboundOnce) {
          $this->changedInboundOnce = TRUE;
          $this->originalRewrittenInboundPath = str_replace(' ', '+', $path);
          $this->replaceQueryParameters($alias, $request);
          $new_path = $system_path_of_alias;
        }
        elseif ($this->originalRewrittenInboundPath == strtok($request->getRequestUri(), '?')) {
          $this->replaceQueryParameters($alias, $request);
          $new_path = $system_path_of_alias;
        }
        $new_path = $new_path;
      };
    }
    return $new_path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if ($this->shouldOutboundPathBeRewritten($request, $path)) {
      $this->setViewsFieldNameMap();
      $this->changedOutboundOnce = TRUE;
      // Prevent redirect from redirect module.
      $request->attributes->set('_disable_route_normalizer', TRUE);
      return $this->sanitizeOutboundPath($request);
    }
    return $path;
  }

  /**
   * Sets the viewsFieldNameMap property.
   *
   * - Should be done early, but only after conditionals pass.
   */
  protected function setViewsFieldNameMap() {
    if ($this->viewsFieldNameMap === FALSE) {
      $this->viewsFieldNameMap = [];
      foreach (preg_split("/\r\n|\n|\r/", trim($this->config->get('views_filter_name_map'))) as $value) {
        $exploded_values = explode('|', $value);

        if (count($exploded_values) === 2) {
          [$k, $v] = $exploded_values;
          $this->viewsFieldNameMap[$k] = $v;
        }
      }
    }
  }

  /**
   * Load paths & views to rewrite from config.
   *
   * @return array
   *   Return paths to rewrite.
   */
  protected function loadPathsViewsToRewrite() {
    $paths_views_to_rewrite = [];
    $paths = $this->config->get('paths') ? $this->config->get('paths') : [];
    foreach ($paths as $path) {
      $paths_views_to_rewrite[$path['path']] = $path;
    }
    return $paths_views_to_rewrite;
  }

  /**
   * Replace the query parameters needed by Views, based on URL path items.
   *
   * @param string $alias
   *   Path alias.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   */
  protected function replaceQueryParameters($alias, Request $request) {
    // Extract parameters the URL.
    if (empty($this->queryParameters)) {
      $this->queryParameters = $this->extractQueryParamsfromUrl($alias, $request->getRequestUri());
    }
    // Replace pager.
    if ($page = $request->query->get('page')) {
      $this->queryParameters['page'] = $page;
    }
    // Insert into request query parameters.
    if (!empty($this->queryParameters)) {
      // Workaround to not apply redirects from Redirect module.
      $this->queryParameters['destination'] = '1';
      $request->query->replace($this->queryParameters);
    }
    // Prevent redirect from redirect module.
    $request->attributes->set('_disable_route_normalizer', TRUE);
  }

  /**
   * Check if path needs to be rewritten according to config.
   *
   * @param string $path
   *   Given path.
   */
  protected function shouldPathBeRewritten($path) {
    foreach (array_keys($this->pathsViewsToRewrite) as $path_to_rewrite) {
      if (
        $path == $path_to_rewrite ||
        strtok($path, '?') == $path_to_rewrite ||
        strpos($path, $path_to_rewrite . $this->filterSubpath) === 0
      ) {
        return $path_to_rewrite;
      }
    }
    return FALSE;
  }

  /**
   * Is filter in path.
   *
   * @param string $alias
   *   Alias string.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   */
  protected function isFilterInPath($alias, Request $request) {
    if (strpos($request->getRequestUri(), $alias . $this->filterSubpath) === 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Should URL rewriting happen for the outbound path.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   * @param string $path
   *   Given path.
   */
  protected function shouldOutboundPathBeRewritten(Request $request = NULL, $path) {
    if (
    // Must be a valid request object (sometimes null).
      $request
      && $this->shouldPathBeRewritten($path)
    // Both full Request URI and path must pass rewrite condition.
      && $this->shouldPathBeRewritten($request->getRequestUri())
    // The path must be part of full request URI.
      && strpos($request->getRequestUri(), $path) !== FALSE
      && !$this->changedOutboundOnce
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Extract parameters to inject into the request query string from the path.
   *
   * @param string $alias_to_rewrite
   *   Alias to rewrite.
   * @param string $path
   *   Given path.
   */
  protected function extractQueryParamsfromUrl($alias_to_rewrite, $path) {
    $path_items = $this->explodeTranslatePathItems($alias_to_rewrite, $path);
    $filters_data = $this->getExposedFiltersDataByAlias($alias_to_rewrite);
    // Convert path items to array of associative arrays.
    $associative_path_items = [];
    foreach ($path_items as $key => $path_item) {
      if ($key % 2 == 0 && isset($path_items[$key + 1]) && isset($filters_data[$path_item])) {
        $associative_path_items[$path_item] = $path_items[$key + 1];
      }
    }
    // Transform raw filter values to values suitable for the request query parameters.
    $extracted_query_params = [];
    foreach ($filters_data as $filter_name => $filter_data) {
      $raw_value_string = $associative_path_items[$filter_name] ?? '';
      $extracted_query_params[$filter_name] = $this->transformFilterValuesForQuery($raw_value_string, $filter_data);
    }
    return $extracted_query_params;
  }

  /**
   * Extract the path item names based on field name map.
   *
   * @param string $alias_to_rewrite
   *   Alias string.
   * @param string $path
   *   Given path.
   */
  protected function explodeTranslatePathItems($alias_to_rewrite, $path) {
    $path_items = explode('/', strtok(ltrim(str_replace($alias_to_rewrite . $this->filterSubpath, '', $path), '/'), '?'));
    foreach ($path_items as $key => $path_item) {
      if ($key % 2 == 0 && isset($path_items[$key + 1]) && in_array($path_item, $this->viewsFieldNameMap)) {
        $path_items[$key] = array_search($path_item, $this->viewsFieldNameMap);
      }
    }
    return $path_items;
  }

  /**
   * Get the data of exposed filters of a view by alias.
   *
   * @param string $alias
   *   Given alias string.
   *
   * @return array
   *   Keyed by filter identifier, values are plugin_id
   */
  protected function getExposedFiltersDataByAlias($alias) {
    return $this->getExposedFiltersDataByView($this->pathsViewsToRewrite[$alias]);
  }

  /**
   * Get exposed data on filters by view.
   *
   * - Each filter data item is keyed by 'vid' & 'plugin_id'
   *
   * @param string|array $paths_view_item
   *   Views id.
   */
  protected function getExposedFiltersDataByView($paths_view_item) {
    $view = $this->entityTypeManager->getStorage('view')->load($paths_view_item['view']);
    $filters = $view->getDisplay($paths_view_item['display'])['display_options']['filters'] ?? NULL;
    $default_filters = $view->getDisplay('default')['display_options']['filters'];
    $filters = array_merge($default_filters, $filters ? $filters : []);
    $exposed_filters = [];
    foreach ($filters as $filter) {
      if (isset($filter['exposed']) && $filter['exposed']) {
        $exposed_filters[$filter['expose']['identifier']] = [
          'plugin_id' => $filter['plugin_id'],
        ];
        if (isset($filter['vid'])) {
          $exposed_filters[$filter['expose']['identifier']]['vid'] = $filter['vid'];
        }
      }
    }
    return $exposed_filters;
  }

  /**
   * Sanitize initial path of request object.
   *
   * - Replaces + signs w/ ' ' because Drupal redirects infinitely
   *   if + signs are in the path.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   */
  protected function sanitizeOutboundPath(Request $request) {
    // Using urldecode() prevents infinite redirects.
    // Prevent two '/' characters in front of the path.
    return '/' . ltrim(urldecode(strtok(str_replace('+', ' ', $request->getRequestUri()), '?')), '/');
  }

  /**
   * Convert the filter value string into value format.
   *
   * @param string $filter_value_string
   *   String to filter.
   * @param array $filter_data
   *   Requires a 'plugin_id' array key.
   */
  protected function transformFilterValuesForQuery($filter_value_string, array $filter_data) {
    if (array_key_exists($filter_data['plugin_id'], $this->filterHandlers)) {
      return $this->filterHandlers[$filter_data['plugin_id']]->transformPathValueForViewsQuery($filter_value_string, $filter_data);
    }
  }

  /**
   * Modify the query params in the pager URLs for rewritten views.
   *
   * @param array $variables
   *   Drupal render array for the Views pager.
   * @param string $type
   *   Type of pager, particularely handling mini pager.
   */
  public function preProcessPager(array &$variables, $type = 'standard') {
    $currentRequestUri = $this->currentRequest->getRequestUri();
    $rewrite = FALSE;
    if ($this->shouldPathBeRewritten($currentRequestUri)) {
      if ($type == 'mini') {
        $currentRequestUri = strtok($currentRequestUri, '?');
        $rewrite = TRUE;
      }
      $pager_item_names = [
        'first',
        'previous',
        'pages',
        'next',
        'last',
        'current',
      ];
      foreach ($pager_item_names as $name) {
        if (!empty($variables['items'][$name])) {
          $this->preProcessPagerItem($variables['items'][$name], $currentRequestUri, $rewrite);
          if ($name == 'pages') {
            foreach ($variables['items']['pages'] as &$page) {
              $this->preProcessPagerItem($page, $currentRequestUri, $rewrite);
            }
          }
        }
      }
    }
  }

  /**
   * Modify the query params in the URL of a single pager item.
   *
   * @param mixed $pager_item
   *   Pager item.
   * @param string $current_uri
   *   Current uri.
   * @param bool $rewrite
   *   Whether to rewrite the url.
   */
  protected function preProcessPagerItem(&$pager_item, $current_uri, $rewrite) {
    if (!empty($pager_item['href'])) {
      parse_str($pager_item['href'], $query_params);
      if (isset($query_params['page'])) {
        $pager_item['href'] = '?page=' . $query_params['page'];
      }
      if ($rewrite) {
        $pager_item['href'] = $current_uri . '?page=' . $query_params['page'];
      }
    }
  }

  /**
   * Handle views exposed form submit.
   *
   * @param array $form
   *   Exposed form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function handleViewsExposedFormSubmit(array $form, FormStateInterface $form_state) {
    // Check if it's been submitted.
    if ($alias = $this->shouldPathBeRewritten($this->currentRequest->getRequestUri())) {
      if ($this->shouldAliasViewBeRewritten($alias, $form_state->get('view')->id()) && $this->requestHasQueryParams()) {
        $this->formState = $form_state;
        $redirect_response = $this->translateSubmittedValuesIntoRewrittenRedirect($alias);
        $redirect_response->send();
        // Required to prevent infinite redirects.
        exit();
      }
    }
  }

  /**
   * Should the combination of an alias and a view be rewritten.
   *
   * - Controls for multiple views on a page, with different action URLs.
   *
   * @param string $alias
   *   Alias string.
   * @param string $view_id
   *   Views id.
   */
  protected function shouldAliasViewBeRewritten($alias, $view_id) {
    if ($this->pathsViewsToRewrite[$alias]['view'] === $view_id) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Translate the submitted values into the rewritten URL.
   *
   * @param string $alias
   *   Given alias.
   *
   * @return \Drupal\Core\Routing\TrustedRedirectResponse
   *   Redirect.
   */
  protected function translateSubmittedValuesIntoRewrittenRedirect($alias) {
    // Get values from URL parts instead of form state, since they often are not there.
    $url_parts = UrlHelper::parse($this->currentRequest->getRequestUri());
    if (!empty($url_parts['query'])) {
      $raw_values = $url_parts['query'];
      $submitted_values = [];
      // Get filter config data.
      $filter_data = $this->getExposedFiltersDataByView($this->pathsViewsToRewrite[$alias]);
      foreach ($filter_data as $filter_name => $filter) {
        if (empty($raw_values[$filter_name])) {
          continue;
        }
        $submitted_values[$filter_name] = [
          'plugin_id' => $filter['plugin_id'],
        ];
        $submitted_values[$filter_name]['value'] = $raw_values[$filter_name];
      }
      $path_string = '';
      // Arrange submitted values keys alphabetically.
      ksort($submitted_values);
      foreach ($submitted_values as $field_name => $field) {
        $string_prefix = '/' . $this->translateSubmitFieldName($field_name) . '/';
        if (
          (array_key_exists($field['plugin_id'], $this->filterHandlers)) &&
          ($value_for_url = $this->filterHandlers[$field['plugin_id']]->transformSubmittedValueForUrl($field['value']))
        ) {
          $path_string .= $string_prefix . $value_for_url;
        }
      }
      $redirect_path = empty($path_string) ? $alias : $alias . $this->filterSubpath . $path_string;
    }
    $base_url = Url::fromUri('base:/', ['absolute' => TRUE]);
    return new TrustedRedirectResponse(urldecode(rtrim($base_url->toString(), '/') . '/' . ltrim($redirect_path, '/')));
  }

  /**
   * Translate the name of a submitted field in case there is an override.
   *
   * @param string $field_name
   *   Field name.
   */
  protected function translateSubmitFieldName($field_name) {
    if (isset($this->viewsFieldNameMap[$field_name])) {
      return $this->viewsFieldNameMap[$field_name];
    }
    return $field_name;
  }

  /**
   * Determine whether the current request is an old views URL.
   */
  protected function requestHasQueryParams() {
    $get_params = $_GET;
    unset($get_params['page']);
    if (empty($get_params)) {
      return FALSE;
    }
    return TRUE;
  }

}
